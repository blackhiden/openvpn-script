#!/bin/bash
# <spinner>
mainPid=$$
LOOPINTERACTIVE=TRUE
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
scriptLocation=${SCRIPT_DIR:-/root/ovpn}
logfile="$scriptLocation/server.log"
env="$SCRIPT_DIR/.env"
fninstall="$SCRIPT_DIR/fn-install.sh"
d=$(date '+%a, %d %B %Y - %H:%M:%S')
[ -f "$logfile" ] && echo "[$d]" > "$logfile"

UBLACK='\033[4;30m'
UBLUE='\033[4;34m'
UWHITE='\033[4;37m'
URED='\033[4;31m'

BBLACK='\033[1;30m'
BBLUE='\033[1;34m'
BGREEN='\033[1;32m'
BRED='\033[1;31m'
BYELLOW='\033[1;33m' 

NC='\033[0m'

clearLine(){
    echo -en "\033[2K"
}

cursorGoesToUp(){
    for i in $(seq $1); do
        echo -en "\033[s\033[1A\033[K"
    done
}

spin(){
    stringSpin="$@"
    spinner=(⣾ ⣽ ⣻ ⢿ ⡿ ⣟ ⣯ ⣷ )
    tput civis
    while :
    do
        for frame in "${spinner[@]}"
        do
        #   echo -n "${spinner:$i:1}"
        printf "\r%s" "[ ${frame} ] $stringSpin "
        sleep 0.05
        done
    done
}

spinTimer(){
    clearLine
    timer=$1
    stringSpin="$2"
    spinner=(⣾ ⣽ ⣻ ⢿ ⡿ ⣟ ⣯ ⣷ )
    tput civis
    while :
    do
        for sec in $(seq $timer -1 0)
        do
        #   echo -n "${spinner:$i:1}"
        printf "\r%s" "$stringSpin $sec"
        sleep 1
        done
    done
}

startSpin(){
    spin "$@" &
    spinpid=$!
    sleep 1
}

stopSpin(){
    clearLine
    kill $spinpid &> /dev/null
    tput cnorm
}

startSubSpin(){
    clearLine
    kill $spinpid &> /dev/null
    spin "$@" &
    spinpid=$!
    sleep 1
}

stopSpinSuccess(){
    clearLine
    kill $spinpid &> /dev/null
    printf "\r[ ${BGREEN}✔${NC} ] $@\n"
    tput cnorm
    printf "[INFO][$(date)]$@\n" >> "$logfile"
}

stopSpinFailed(){
    clearLine
    kill $spinpid &> /dev/null
    printf "\r[ ${BRED}⨉${NC} ] $@\n"
    tput cnorm
    printf "[ERROR][$(date)]$@\n" >> "$logfile"
}

stopSpinFailedExit1(){
    clearLine
    kill $spinpid &> /dev/null
    printf "\r[ ${BRED}⨉${NC} ] $@\n"
    tput cnorm
    printf "[ERROR][$(date)]$@\n" >> "$logfile"
    exit 1
}


countDown(){
    stringCountDown=$1
    timer=$2
    for sec in $(seq $timer -1 0); do
        printf "\r[ ${BYELLOW}!${NC} ] $stringCountDown ${BGREEN}$sec${NC}"
        sleep 1
    done
}

green(){
    printf "${BGREEN}$1${NC}"
}

yellow(){
    printf "${BYELLOW}$1${NC}"
}

red(){
    printf "${BRED}$1${NC}"
}

bblack(){
    printf "${BBLACK}$1${NC}"
}

bblue(){
    printf "${BBLUE}$1${NC}"
}

ulbred(){
    printf "${BRED}${URED}$1${NC}"
}

error(){
    printf "[ ${BRED}⨉${NC} ] $@\n"
}

success(){
    printf "[ ${BGREEN}✔${NC} ] $@\n"
}

infoAppendString(){
    printf "[ ${BBLUE}i${NC} ] $1\n"
}

warning(){
    printf "[ $(yellow !) ] $1\n"
}
# </spinner>

interactive(){
    printf "[ ${BYELLOW}?${NC} ] $1"
    eval "read -r $2"
    eval ": \${$2:=$3}"
}

runDirectCommand(){
    eval "$@" > /dev/null 2>&1
}

runCommandNoSpin(){
    eval "$1" > /dev/null 2>> $logfile
    STATUS_PROCESS=$?
    if [ $STATUS_PROCESS -ne 0 ]; then
        error "$2"
        printf "[ERROR][$(date)]$2\n" >> "$logfile"
        exit 1
    fi
}

runCmdLoop(){
    # 3rd param is variable name that store loop status.
    # eg if cmd run with no error,
    # the variable will store false
    eval "$3=TRUE"
    eval "$1" > /dev/null 2>> $logfile
    STATUS_PROCESS=$?
    if [ $STATUS_PROCESS -eq 0 ]; then
        eval "$3=FALSE"
    else 
        printf "[ERROR][$(date)]$2\n" >> "$logfile"
        error "$2"
        sleep 2
    fi
}

runCmd(){
    # 3rd param is variable name that store loop status.
    # eg if cmd run with no error,
    # the variable will store false
    eval "$3=FALSE"
    eval "$1" > /dev/null 2>> $logfile
    STATUS_PROCESS=$?
    if [ $STATUS_PROCESS -eq 0 ]; then
        eval "$3=TRUE"
    else 
        printf "[ERROR][$(date)]$2\n" >> "$logfile"
        error "$2"
        sleep 2
    fi
}

runCommandNoSpinLoopInteractiveRev(){
    eval "$1" > /dev/null 2>> $logfile
    STATUS_PROCESS=$?
    if [ $STATUS_PROCESS -eq 0 ]; then
        printf "[ERROR][$(date)]$2\n" >> "$logfile"
        error "$2"
        sleep 3
    else 
        LOOPINTERACTIVE=FALSE
        LOOPINTERACTIVETWO=FALSE
    fi
}


runCommand(){
    eval "$@" > /dev/null 2>> $logfile
    STATUS_PROCESS=$?
    if [ $STATUS_PROCESS -ne 0 ]; then
        stopSpinFailedExit1 "Failed run command ${BBLUE}$@${NC}\n"
    fi
}

runCommandExitMessage(){
	startSubSpin "$2"
    eval "$1" > /dev/null 2>> $logfile
    STATUS_PROCESS=$?
    if [ $STATUS_PROCESS -ne 0 ]; then
		stopSpinFailedExit1 "$4"
	else
		stopSpinSuccess "$3"
    fi
}

runCommandExitMessageRev(){
	startSubSpin "$2"
    eval "$1" >> /dev/null 2>> $logfile
    STATUS_PROCESS=$?
    if [ $STATUS_PROCESS -eq 0 ]; then
		stopSpinFailedExit1 "$4"
	else
		stopSpinSuccess "$3"
    fi
}

randomCharacter(){
    cat /dev/urandom | tr -dc '[:alpha:]' | fold -w ${1:-20} | head -n 1
}

randomCharacterLowerCase(){
    cat /dev/urandom | tr '[:upper:]' '[:lower:]' | tr -dc '[:alpha:]' | fold -w ${1:-20} | head -n 1
}


strToArr(){
    str=${1:-nostr}
    arr=${2:-novar}
    dlmtr=${3:-nodlmtr}
    if [ ! -z $dlmtr ] && [ $dlmtr != 'nodlmtr' ]; then
        dlmtr="$3"
    else
        dlmtr=" "
    fi
    if [ ! -z "$str" ]; then
        eval "IFS=\"$dlmtr\" read -r -a $arr <<< \"$str\" "
    else
        exit 1
    fi
}

# <netcalculator>
tonum() {
    if [[ $1 =~ ([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+)\.([[:digit:]]+) ]]; then
        addr=$(( (${BASH_REMATCH[1]} << 24) + (${BASH_REMATCH[2]} << 16) + (${BASH_REMATCH[3]} << 8) + ${BASH_REMATCH[4]} ))
        eval "$2=\$addr"
    fi
}
toaddr() {
    b1=$(( ($1 & 0xFF000000) >> 24))
    b2=$(( ($1 & 0xFF0000) >> 16))
    b3=$(( ($1 & 0xFF00) >> 8))
    b4=$(( $1 & 0xFF ))
    eval "$2=\$b1.\$b2.\$b3.\$b4"
}

ipsubnet(){
    # convert ip format to network id format
    # $1 = ip address
    # $2 = netmask in netmask format
    # $3 = variable name containing network id
    # $4 = variable name containing broadcast
    # $5 = variable name containing the number of network id
    # $6 = variable name containing the number of broadcast
    tonum $1 IPADDRNUM
    tonum $2 NETMASKNUM
    varNet="${3:--}"
    varBc="${4:--}"
    varNetNum="${5:--}"
    varBcNum="${6:--}"
    # The logic to calculate network and broadcast
    INVNETMASKNUM=$(( 0xFFFFFFFF ^ NETMASKNUM ))
    NETWORKNUM=$(( IPADDRNUM & NETMASKNUM ))
    BROADCASTNUM=$(( INVNETMASKNUM | NETWORKNUM ))
    toaddr $NETWORKNUM networkTmp
    toaddr $BROADCASTNUM broadcastTmp
    if [ ! -z $varNet ] && [ $varNet != "-" ]; then
        eval "$varNet=\$networkTmp"
    fi
    if [ ! -z $varBc ] && [ $varBc != "-" ]; then
        eval "$varBc=\$broadcastTmp"
    fi
    if [ ! -z $varNetNum ] && [ $varNetNum != "-" ]; then
        eval "$varNetNum=\$NETWORKNUM"
    fi
    
    if [ ! -z $varBcNum ] && [ $varBcNum != "-" ]; then
        eval "$varBcNum=\$BROADCASTNUM"
    fi
}

prefixtonetmask(){
    # change prefix to netmask
    # $1 = netmask in cidr format
    # $2 = variable name containing netmask in subnetmask format
    # $3 = variable name containing the number of netmask
    prefix=$1
    zeros=$((32-prefix))
    NETMASKNUM=0
    for (( i=0; i<$zeros; i++ )); do
        NETMASKNUM=$(( (NETMASKNUM << 1) ^ 1 ))
    done
    NETMASKNUM=$((NETMASKNUM ^ 0xFFFFFFFF))
    toaddr $NETMASKNUM netmask
    if [ ! -z $2 ] && [ $2 != "-" ]; then
        eval "$2=\"$netmask\""
    fi
    if [ ! -z $3 ]; then eval "$3=\"$NETMASKNUM\""; fi
}
# </netcalculator>

checkReq(){
    # <preparing>
    startSpin "Checking requirements"
        version=$(grep '^NAME' /etc/os-release)
        version=${version,,}
        ubuntu='Ubuntu'
        debian='Debian'
        
        runCommandExitMessage "\$(echo $version | grep -q 'ubuntu' || echo $version | grep -q 'debian')" "Checking operating system" "Operating system meet requirement (Ubuntu/Debian)" "Your operating system is not Ubuntu nor Debian"
        : ${SUDO_USER:=nosudo}
        # Checking root level
        runCommandExitMessage "[ \"$SUDO_USER\" == \"nosudo\" ]" "Checking if this script run by sudo" "Script run without sudo. Continue!" "Script run by sudo. You should become root"
        runCommandExitMessage "[ $EUID -eq 0 ]" "Checking if this script run by root" "Script run by root. Continue!" "You're not root\n      This script should be run as root in order to work properly"
    stopSpinSuccess "Your system meet requirement"
    # </preparing>
}

banner(){
    clear
    printf "┌------------------------------------------┐
| ${BBLUE}${UBLUE}Simple OpenVPN Script by Denny Lastpaste${NC} |
|                                          |
| ${BGREEN}Usage:${NC}                                   |
| ${BGREEN}./$(basename $0)${NC}                         |
|                                          |
|       ${BRED}NOTE: Debian and Ubuntu only${NC}       |
└------------------------------------------┘
"
# </banner>
}

liner(){
    echo "--------------------------------------------"
}

exit0(){
    exit 0
}

varChecker(){
    startSubSpin "Sourcing $env"
    if [ -f "$env" ]; then
        if source "$env"; then
            startSubSpin "Validate parameters on $env"
            if  [ ! -z $publicIp ] && 
                [ ! -z $localIp ] && 
                [ ! -z $protocol ] && 
                [ ! -z $port ] && 
                [ ! -z $configDirectory ] && 
                [ ! -z $tlsStatus ] && 
                [ ! -z $serverCN ] && 
                [ ! -z $etc ] && 
                [ ! -z $ccd ] && 
                [ ! -z $route ] && 
                [ ! -z $ta ] && 
                [ ! -z $crl ] && 
                [ ! -z $clientConfig ] && 
                [ ! -z $easyrsaGit ] && 
                [ ! -z $easyrsa ] && 
                [ ! -z $easyrsaCrl ] && 
                [ ! -z $indexEasyRsa ] && 
                [ ! -z $privateKey ] && 
                [ ! -z $privateCert ] && 
                [ ! -z $privateReq ] && 
                [ ! -z $management_host ] && 
                [ ! -z $management_port ] && 
                [ ! -z $logfile ] && 
                [ ! -z $tlsStatus ] &&
                [ ! -z $shellUserReadConfig ] && 
                [ ! -z $dirUserReadConfig ]; then
                    stopSpinSuccess "Sourcing success"
            else
                stopSpinFailedExit1 "Some of variable are gone. Please see .env.example as reference"
            fi
        else
            stopSpinFailedExit1 "Sourcing failed"
        fi
    else
        stopSpinFailedExit1 "$env not found"
    fi
}

sourcingEnv(){
    startSubSpin "Sourcing $env"
    if [ -f "$env" ]; then
        if source "$env"; then
            stopSpinSuccess "Sourcing success"
        else
            stopSpinFailed "Sourcing failed"
        fi
    else
        stopSpinFailed "$env not found"
    fi
}

showMenu(){
    if [ -f "$SCRIPT_DIR/.env" ]; then
    banner
# printf "%s----------------------------------------------\n"
printf "[ $(bblue ?) ] What will you do?\n"
printf "%s----------------------------------------------\n"
printf "[ $(bblue 1) ] Create Client
[ $(bblue 2) ] List User
[ $(bblue 3) ] Read Client Configuration
[ $(bblue 4) ] Delete User
[ $(bblue 99) ] Advanced
[ $(bblue \*) ] Exit\n"
printf "%s----------------------------------------------\n"
printf "[ $(bblue !) ] Enter the number you want to excute: "
read CHOOSEMENU
case $CHOOSEMENU in
	1) source "$SCRIPT_DIR/fn/genovpn.sh" && genconfig ;; 
	2) source "$SCRIPT_DIR/fn/listuser.sh" && listUserMenu ;; 
	3) source "$SCRIPT_DIR/fn/clientconfig.sh" && readClientConfig ;; 
	4) source "$SCRIPT_DIR/fn/delete.sh" && delete ;; 
    99) advancedMenu ;;
	*) exit 0;;
esac
    else
# printf "%s----------------------------------------------\n"
printf "[ ${BBLACK}?${NC} ] What will you do?\n"
printf "%s----------------------------------------------\n"
printf "[ ${BBLACK}1${NC} ] Install OpenVPN
[ ${BBLACK}*${NC} ] Exit\n"
printf "%s----------------------------------------------\n"
printf "[ ${BBLACK}!${NC} ] Enter the number you want to excute: "
read CHOOSEMENU
case $CHOOSEMENU in
	1) source "$SCRIPT_DIR/fn/install.sh" && install ;; 
	*) exit 0;;
esac
    fi
}

advancedMenu(){
    banner
    printf "[ $(bblue ?) ] [Advanced Menu] What will you do?\n"
printf "%s----------------------------------------------\n"
printf "[ $(bblue 1) ] Push Route
[ $(bblue 2) ] List Route
[ $(bblue 3) ] Delete Route
[ $(bblue 4) ] Reinit Server
[ $(bblue 5) ] Re-install Server
[ $(bblue 00) ] Back
[ $(bblue \*) ] Exit\n"
printf "%s----------------------------------------------\n"
printf "[ $(bblue !) ] Enter the number you want to excute: "
read CHOOSEMENU
case $CHOOSEMENU in
	1) source "$SCRIPT_DIR/fn/pushroute.sh" && pushRoute ;; 
    2) source "$SCRIPT_DIR/fn/pushroute.sh" && listRoutes ;;
    3) source "$SCRIPT_DIR/fn/pushroute.sh" && deleteRoute ;;
	4) source "$SCRIPT_DIR/fn/install.sh" && reinitServer ;; 
	5) source "$SCRIPT_DIR/fn/install.sh" && install ;; 
    00) showMenu ;;
	*) exit 0;;
esac
}

showMenu