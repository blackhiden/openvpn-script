install(){
    banner
    checkReq
    runCommandExitMessageRev "systemctl list-units --full -all | grep -F 'openvpn@server.service' | grep -Fq 'running'" "Checking if openvpn@server.service already running" "All clean. Continue to install" "openvpn@server.service already running\n      Please stop and disable the service\n      by run the following commands\n      $(yellow 'systemctl stop openvpn@server.service && systemctl disable openvpn@server.service')"
    runCommandExitMessage "ping -c 3 8.8.8.8" "Checking internet connection" "Connected to the internet!" "You're not connected to the internet (8.8.8.8)"
    startSpin "Installing additional package"
        runCommand "apt update -y"
        if ! dpkg -s debconf-utils > /dev/null 2>&1; then
            runCommand "apt install debconf-utils -y"
        fi
        if ! dpkg -s netcat > /dev/null 2>&1; then
            if apt-cache search netcat | grep -q "netcat - "; then
                runCommand "apt install netcat -y"
            else
                runCommand "apt install netcat-traditional -y"
            fi 
        fi
        if ! dpkg -s iptables > /dev/null 2>&1; then
            runCommand "apt install iptables -y"
        fi
        if ! dpkg -s iptables-persistent > /dev/null 2>&1; then
            echo "iptables-persistent     iptables-persistent/autosave_v4 boolean true" | debconf-set-selections
            echo "iptables-persistent     iptables-persistent/autosave_v6 boolean true" | debconf-set-selections
            runCommand "apt install iptables-persistent -y"
        fi
    stopSpinSuccess "Installing additional package success!"
    defaultLink=$(ip ro sh | grep default | head -n1 | cut -d' ' -f5)
    ipSubnet=$(ip ad sh | grep $defaultLink | grep inet | head -n1 | tr -s ' ' |cut -d' ' -f3)
    IpDefaultLink=$(grep -oP "^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}" <<< $ipSubnet)
    curlIpPublic=$(curl -fsSL icanhazip.com)
    defConfigDirectory="/root/$(randomCharacterLowerCase 5)"
    defLocalIp="192.168.$(( (RANDOM % 254)  )).0"
    defPort="1194"
    defPortSsh="22"
    defIpSsh=$curlIpPublic
    defProtocol="udp"
    defProtocolNumber="1"
    defTlsStatus="no"
    defRegUser=$(randomCharacterLowerCase 5)
    defRegUserDir=$(randomCharacterLowerCase 5)
    banner
	echo "---------------- I N F O ------------------"
    echo "The following value are default values used"
    echo "by installation script. You can use those"
    echo "parameters by leaving it empty while in"
    echo "interactive or fill it with your preferences"
    liner
    infoAppendString "Script Location: $(yellow $scriptLocation)"
    infoAppendString "Configuration Location: $(yellow $defConfigDirectory)"
    infoAppendString "Log file: $(yellow $logfile)"
    infoAppendString "User: $(yellow $defRegUser)"
    infoAppendString "User Config Directory: $(yellow /home/$defRegUser/$defRegUserDir)"
    infoAppendString "Gateway link: $(yellow $defaultLink)"
    infoAppendString "Local IP Address: $(yellow $IpDefaultLink)"
    infoAppendString "OpenVPN Public IP: $(yellow $curlIpPublic)"
    infoAppendString "Openvpn Local Network: $(yellow $defLocalIp)"
    infoAppendString "Protocol: $(yellow $defProtocol)"
    infoAppendString "Port: $(yellow $defPort)"
    infoAppendString "TLS: $(yellow No)"
    liner
    # Interactive OpenVPN Public IP
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter OpenVPN Public IP (client connect to)\n      or leave it empty to use $(yellow $curlIpPublic) as Public IP: " "publicIp" "$curlIpPublic"
        runCmdLoop "echo \"$publicIp\" | grep -qP \"^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}\$\" " "OpenVPN Public IP is invalid\n      Enter valid OpenVPN Public IP" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 4; fi
    done
    infoAppendString "OpenVPN Public IP: $(green $publicIp)"
    # Interactive Openvpn Local Network
    LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "$(ulbred NOTE:) This OpenVPN script will use $(yellow subnet) as topology\n      It means, your OpenVPN Local Network will use $(yellow /24) block IP\n      and $(yellow 0) should be the last octet of the IP\n      Enter Openvpn Local Network\n      or leave it empty to use $(yellow $defLocalIp) as Local IP: " "localIp" "$defLocalIp"
        checkIp=$(grep -oP "^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){3}\.[0-9]" <<< $localIp)
        runCmdLoop "echo \"$localIp\" | grep -qP \"^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){3}\.0\$\" && ! \$(ip route show | grep -qP \"$localIp\") " "Openvpn Local Network is invalid or it being used by other service\n      Enter valid Openvpn Local Network" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 5; else cursorGoesToUp 6; fi
    done
    infoAppendString "OpenVPN Local Network: $(green $localIp)"
    # Interactive Openvpn Protocol
    LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Available protocol:\n      1. udp\n      2. tcp\n      Due to network quality, It's recommended to use $(yellow udp)\n      Enter OpenVPN Protocol number\n      or leave it empty to use $(yellow $defProtocol) as Protocol: " "protocolNumber" "$defProtocolNumber"
        runCmdLoop "echo \"$protocolNumber\" | grep -qP \"[1-2]\" " "Openvpn protocol is invalid\n      Enter valid Openvpn protocol" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 6; else cursorGoesToUp 8; fi
    done
    if [ $protocolNumber == "1" ]; then protocol=udp; else protocol=tcp; fi
    infoAppendString "OpenVPN Protocol: $(green $protocol)"
    # Interactive Openvpn Port
    LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter OpenVPN Port (1-65535)\n      or leave it empty to use $(yellow $defPort): " "port" "$defPort"
        runCmdLoop "echo \"$port\" | grep -qP \"^(0|[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$\" && ! nc -z -w 3 127.0.0.1 $port" "Openvpn port is invalid or it already has been used. Enter valid and free Openvpn port" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
    done
    infoAppendString "OpenVPN Port: $(green $port)"
    infoAppendString "OpenVPN TLS: $(green NO) ($(ulbred NOTE:) This option still under development)"
    LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter configuration directory\n      (This directory used for saving client configuration and certificate)\n      or leave it empty to use $(yellow $defConfigDirectory): " "configDirectory" "$defConfigDirectory"
        runCmdLoop "[ ! -d $configDirectory ] || [ ! -z $(ls -A $configDirectory) ]" "Directory is not empty. Use another directory" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 4; else cursorGoesToUp 4; fi
    done
    infoAppendString "Configuration directory: $(green $configDirectory)"
    
    while [ -z $userValidChar ] || [ $userValidChar == "TRUE" ]; do
        interactive "In order to make it easier (rather safer)\n      we need to copy configuration into\n      directory belongs to a regular user.\n      Please enter regular user [default: $(yellow $defRegUser)]: " "shellUserReadConfig" "$defRegUser"
        runCmdLoop "echo $shellUserReadConfig | grep -qP '^[a-z]*$'" "Invalid character. a-z only" "userValidChar"
        if [ $userValidChar == "FALSE" ]; then cursorGoesToUp 4; else cursorGoesToUp 5; fi
    done
    # define ssh and address
    LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "$(yellow NOTE:) Due to security reason\n      All distribution config should be done via SSH\n      In this context, all config will be delivered via SCP\n      Enter SSH IP address\n      or leave it empty to use $(yellow $defIpSsh) as IP SSH server: " "sshAddress" "$defIpSsh"
        checkIp=$(grep -oE "\b((25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)\b" <<< $sshAddress)
        runCmdLoop "echo \"$sshAddress\" | grep -qE \"\b((25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)\.){3}(25[0-5]|2[0-4][0-9]|1?[0-9][0-9]?)\b\" " "SSH IP address is invalid\n      Enter valid SSH IP address" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 6; else cursorGoesToUp 7; fi
    done
    # define ssh and port
    LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter SSHP Port (1-65535)\n      or leave it empty to use $(yellow $defPortSsh): " "sshPort" "$defPortSsh"
        runCmdLoop "echo \"$sshPort\" | grep -qP \"^(0|[1-9][0-9]{0,3}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$\" && nc -zw 3 127.0.0.1 $sshPort" "SSH port is invalid or not activated. Enter valid and activate SSH service" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
    done
    if ! id -u $shellUserReadConfig > /dev/null 2>&1 ; then
        infoAppendString "Username does not exist. It will create a new user called $(green $shellUserReadConfig)"
        while [ -z $loopPassword ] || [ $loopPassword == "TRUE" ]; do
            interactive "Enter password for $(yellow $shellUserReadConfig) user [default: $(yellow ovpnconfig)]: " "firsPassword" "ovpnconfig"
            interactive "Enter again password for $(yellow $shellUserReadConfig) user [default: $(yellow ovpnconfig)]: " "secondPassword" "ovpnconfig"
            runCmdLoop "[ $firsPassword == $secondPassword ]" "Password does not match" "loopPassword"
            if [ $loopPassword == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
        done
        infoAppendString "Your password for new user is: $(green $secondPassword)"
        runCommandExitMessage "useradd -s /bin/bash -d /home/$shellUserReadConfig -m $shellUserReadConfig" "Adding user..." "Adding user success" "Adding user failed"
        runCommandExitMessage "echo -e \"$secondPassword\n$secondPassword\" | passwd $shellUserReadConfig" "Adding password into user" "Adding password into user success" "Adding password into user failed"
    fi

    ovpnDirRegUserAppend=$(grep "$shellUserReadConfig" /etc/passwd | cut -d':' -f6)
    infoAppendString "As it said that openvpn config\n      will be copied into directory belongs to $(yellow $shellUserReadConfig)\n      we need to set directory name\n      and put it into home directory.\n      In this case, your directory will be placed into\n      $(yellow $ovpnDirRegUserAppend/$defRegUserDir)"
    while [ -z $loopOvpnDirRegUser ] || [ $loopOvpnDirRegUser == "TRUE" ]; do
        interactive "Enter name of directory for $(yellow $shellUserReadConfig) user [default: $(yellow $defRegUserDir)]: " "dirName" "$defRegUserDir"
        runCmdLoop "echo $dirName | grep -qP '^[a-z]*$'" "Invalid character. a-z only" "loopOvpnDirRegUser"
        if [ $loopOvpnDirRegUser == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
    done
    dirUserReadConfig="$ovpnDirRegUserAppend/$dirName"
    infoAppendString "Please remember, both .ovpn and .pem files\n      will be stored in $dirUserReadConfig"
    if [ -d $dirUserReadConfig ]; then
        infoAppendString "$dirUserReadConfig already exist. Any existing config will be overided"
    else
        runCommandExitMessage "mkdir -p \"$dirUserReadConfig\" " "Creating client config directory" "Creating client config directory success" "Creating client config directory failed"
        runCommandExitMessage "chown $shellUserReadConfig:$shellUserReadConfig \"$dirUserReadConfig\" " "Changing directory ownership" "Changing directory ownership success" "Changing directory ownership failed"
    fi

    #path
    etc="/etc/openvpn"
    ccd="$etc/ccd"
    route="$etc/route.conf"
    ta="$etc/ta.key"
    crl="$etc/crl.key"
    clientConfig="$configDirectory/client-config"
    easyrsaGit="$configDirectory/easyrsa"
    easyrsa="$easyrsaGit/easyrsa3"
    easyrsaCrl="$easyrsa/pki/crl.pem"
    indexEasyRsa=$easyrsa/pki/index.txt
    privateKey=$easyrsa/pki/private
    privateCert=$easyrsa/pki/issued
    privateReq=$easyrsa/pki/reqs
    privateInline=$easyrsa/pki/inline
    management_host=localhost
    management_port=5555
    serverCN="server"
    #additional vars
    tlsStatus="no"
    tls="0"
    keydirection_server=''
    tlskey=''
    banner
    startSpin "Installation begin"
        startSubSpin "Creating directory"
        if [ ! -d "$ccd" ]; then
            mkdir -p "$ccd"
        fi
        if [ ! -f "$route" ]; then
            touch "$route"
        fi
        # create directory - configuration directory
        if [ ! -d "$configDirectory" ]; then
            mkdir -p "$configDirectory"
        fi
        # create directory - client config
        if [ ! -d "$clientConfig" ]; then
            mkdir -p "$clientConfig"
        fi
        # create directory - easyrsa
        if [ ! -d "$easyrsaGit" ]; then
            mkdir -p "$easyrsaGit"
        fi
        runCommandExitMessage "apt-get update -y && apt-get install -y openvpn git telnet python3 python3-full python3-pip bsdmainutils python3-decouple" "Update repository and install main package" "Update repository and install main package success" "Failed to update repository or install main package"
        # runCommandExitMessage "pip3 install python-decouple" "Installing PIP package" "Installing support package success" "Failed to install support package success"
        runCommandExitMessage "git clone https://github.com/OpenVPN/easy-rsa.git $easyrsaGit" "Clonning EasyRSA..." "EasyRSA has been cloned" "Failed to clone EasyRSA"
        initOvpnServer
    exit 0
}

initOvpnServer(){
    cd $easyrsa
    runCommandExitMessage "./easyrsa init-pki" "EasyRSA initiation" "Initiation success" "Initiation failed"
    if [ ! -f "$easyrsa/pki/openssl-easyrsa.cnf" ]; then
        cp "$easyrsa/openssl-easyrsa.cnf" "$easyrsa/pki/openssl-easyrsa.cnf"
    fi
    # create credential
    cat << EOF > $easyrsa/vars
set_var EASYRSA_KEY_SIZE    2048
set_var EASYRSA_ALGO        "ec"
set_var EASYRSA_DIGEST      "sha256"
set_var EASYRSA_BATCH       "1"
set_var EASYRSA_REQ_COUNTRY "Indonesia"
set_var EASYRSA_REQ_PROVINCE "East Java"
set_var EASYRSA_REQ_CITY    "Jember"
set_var EASYRSA_REQ_ORG     "Personal"
set_var EASYRSA_REQ_EMAIL   "blackhiden@gmail.com"
set_var EASYRSA_REQ_OU      "Personal"
set_var EASYRSA_OPENSSL     "openssl"
set_var EASYRSA_ALGO	    rsa

set_var EASYRSA			"\${0%/*}"
set_var EASYRSA_OPENSSL		"openssl"
set_var EASYRSA_PKI		"\$PWD/pki"
set_var EASYRSA_TEMP_DIR	"\$EASYRSA_PKI"
set_var EASYRSA_DN		"cn_only"
set_var EASYRSA_REQ_COUNTRY	"Indonesia"
set_var EASYRSA_REQ_PROVINCE	"East Java"
set_var EASYRSA_REQ_CITY	"Jember"
set_var EASYRSA_REQ_ORG		"Personal"
set_var EASYRSA_REQ_EMAIL	"blackhiden@gmail.com"
set_var EASYRSA_REQ_OU		"Personal"
set_var EASYRSA_KEY_SIZE	2048
set_var EASYRSA_ALGO		rsa
set_var EASYRSA_CURVE		secp384r1
set_var EASYRSA_CA_EXPIRE	3650
set_var EASYRSA_CERT_EXPIRE	3650
set_var EASYRSA_CRL_DAYS	3650
set_var EASYRSA_CERT_RENEW	3650
set_var EASYRSA_RAND_SN		"yes"
set_var EASYRSA_NS_SUPPORT	"no"
set_var EASYRSA_NS_COMMENT	"Easy-RSA Generated Certificate"
set_var EASYRSA_SSL_CONF	"\$EASYRSA_PKI/openssl-easyrsa.cnf"
set_var EASYRSA_DIGEST		"sha256"
set_var EASYRSA_BATCH		"1"

EOF
    cd $easyrsa
    runCommandExitMessage "./easyrsa build-ca nopass" "Build Certificate Authority" "Build Certificate Authority success" "Build Certificate Authority failed"
    runCommandExitMessage "./easyrsa gen-dh" "Build Diffie Hellman" "Build Diffie Hellman success" "Build Diffie Hellman failed"
    runCommandExitMessage "./easyrsa build-server-full server nopass" "Build Server certificate" "Build Server certificate success" "Build Server certificate failed"
    runCommandExitMessage "./easyrsa gen-crl" "Generate Certificate Revocation List" "Generate Certificate Revocation List success" "Generate Certificate Revocation List failed"
    cp "$easyrsaCrl" "$crl"
    # [ ! -f "$route/route.conf" ] && touch "$route/route.conf"
    if [ -z $tlsStatus ] || [ $tlsStatus == "no" ] || [ $defTlsStatus == "no" ]; then
        keydirection_server=''
        tlskey=''
    else
        runCommandExitMessage "/usr/sbin/openvpn --genkey --secret \"$etc/ta.key\" " "Generate secret key" "Generate secret key success" "Generate secret key failed"
        keydirection_server="0"
        read -r -d '' tlskey << EOF
<tls-auth>
$(cat $ta)
</tls-auth>
EOF
    fi
    cat << EOF > $etc/$serverCN.conf
# ------------- C O P Y  H E R E -------------
# push "ping-exit 60" # enable this option if you want to shutdown client with no activity
# ping 3
# ping-restart 3

dev tun
topology subnet
server $localIp 255.255.255.0
proto ${protocol}4
port $port
keepalive 10 60
user nobody
group nogroup
status /var/log/openvpn.log
verb 3
persist-tun
persist-key
crl-verify $crl
auth SHA1
cipher AES-128-CBC
data-ciphers AES-128-CBC
$keydirection_server
config $route
client-to-client
client-config-dir $ccd
management localhost 5555
<ca>
$(cat $easyrsa/pki/ca.crt)
</ca>
<key>
$(cat $easyrsa/pki/private/$serverCN.key)
</key>
<cert>
$(cat $easyrsa/pki/issued/$serverCN.crt)
</cert>
<dh>
$(cat $easyrsa/pki/dh.pem)
</dh>
# ------------- E N D S  H E R E -------------
EOF

    cat << EOF > $SCRIPT_DIR/.env
# ------------- C O P Y  H E R E -------------
# change this only
publicIp="$publicIp"
localIp="$localIp"
protocol="$protocol"
port="$port"
configDirectory="$configDirectory"
tlsStatus="$tlsStatus"
serverCN="$serverCN"
shellUserReadConfig=$shellUserReadConfig
sshPassword=$secondPassword
dirUserReadConfig=$dirUserReadConfig
sshAddress=$sshAddress
sshPort=$sshPort

# change this may break the script
etc="$etc"
ccd="$ccd"
route="$route"
ta="$ta"
crl="$crl"
clientConfig=$clientConfig
easyrsaGit="$easyrsaGit"
easyrsa="$easyrsa"
easyrsaCrl="$easyrsaCrl"
indexEasyRsa="$indexEasyRsa"
privateKey="$privateKey"
privateCert="$privateCert"
privateReq="$privateReq"
privateInline="$privateInline"
management_host="$management_host"
management_port="$management_port"
logfile="$logfile"
# ------------- E N D S  H E R E -------------
EOF
    if [ -f /etc/sysctl.conf ]; then sed -i 's/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/' /etc/sysctl.conf; fi
    if [[ ! -z $defaultLink ]]; then
        if iptables -t nat -vnL POSTROUTING 1 | grep -qE "MASQUERADE.*$defaultLink";then
            iptables -t nat -I POSTROUTING 1 -o $defaultLink -j MASQUERADE
        fi
        iptables-save > /etc/iptables/rules.v4
    fi
    runCommandExitMessage "systemctl stop openvpn@$serverCN.service && systemctl start openvpn@$serverCN.service" "Starting OpenVPN service" "Starting OpenVPN service success" "Starting OpenVPN service failed"
    runCommandExitMessage "systemctl disable openvpn@$serverCN.service && systemctl enable openvpn@$serverCN.service" "Enabling OpenVPN service" "Enabling OpenVPN service success" "Enabling OpenVPN service failed"
    stopSpin "Installation success!"
}

reinitServer(){
    banner
    LOOPINTERACTIVE=TRUE
	while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "$(ulbred NOTED:) This action will remove configuration and disconnect all clients\n      Are you sure you want to re-init server?(y/N)\n      or leave it empty to exit: " "reinitServerChar" "n"
		runCmdLoop "echo $reinitServerChar | grep -qP \"^(y|Y|yes|Yes|YEs|YES|yES|yeS|yEs|n|N|no|No|NO|nO)\$\" " "Please write Yes or No" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 3; else cursorGoesToUp 4; fi
	done
	reinitServer=${reinitServerChar,,}
	if [[ "$reinitServer" =~ ^(no|n)$ ]]; then infoAppendString "No action. Exit!"; exit 0; fi
    checkReq
    varChecker
    if [ ! -z $ccd ] && ls -A $ccd/* > /dev/null 2>&1; then
        runCommandExitMessage "rm $ccd/*" "Removing client config files" "Removing client config files success" "Removing client config files failed"
    fi
    initOvpnServer
    exit 0
}