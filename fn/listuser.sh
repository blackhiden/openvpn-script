listuser(){
    pythonOutput=$(python3 - << EOF
import unicodedata
import re
import getpass
import telnetlib
import time
import os
from decouple import config

HOST = unicodedata.normalize('NFKD', config('management_host')).encode('ascii', 'ignore')
PORT = unicodedata.normalize('NFKD', config('management_port')).encode('ascii', 'ignore')

# timeout = 30
# user = input("Enter your remote account: ")
# password = getpass.getpass("User Password: ")
# enable = getpass.getpass("Enable Password: ")

tn = telnetlib.Telnet(HOST,PORT)

tn.write(b"status\n")
# tn.write(b"exit\n")
time.sleep(3)
s = tn.read_very_eager()
string_output = s.decode('ascii')
print(string_output)
tn.close()
EOF
)

    list_=$(printf "$pythonOutput"  | sed '1,4d' | sed '/^ROUTING/,$d' | sed 's/\:.*$//g')
    initNumber=1
    while IFS= read -r l; do
        if ! echo $l | grep -qEo "^.*/CN=$serverCN$"; then
            ipaddress=""
            username=$(echo $l | sed -rn 's/^.*\/CN=([A-Za-z0-9]*)$/\1/p')
            active="No"
            publicIpClient="N/A"

            if [ -f "$ccd/$username" ]
            then
                ipaddress=$(cat "$ccd/$username" | grep -Eo '\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b' | head -n1)
            else
                ipaddress="no_ip_address_assigned"
            fi

            status=""

            if grep -qEo '^R' <<< $l 
            then
                status="Revoke"
            else
                status="Valid"
            fi

            # check if user is online
            for list in $list_; do
                usernameFromList=$(echo $list | cut -d"," -f1)
                publicipClientFromList=$(echo $list | cut -d"," -f2)
                if [[ $username == $usernameFromList ]]; then
                    active="Yes"
                    publicIpClient=$(echo $publicipClientFromList)
                    break
                fi
            done
            if [ -z "$ip_listVar" ]; then
                read -r -d '' ip_listVar <<EOF
# Username IP Active Public Status
- -------- -- ------ ------ ------
$initNumber $username $ipaddress $active $publicIpClient $status
EOF
            else
                read -r -d '' ip_listVar <<EOF
$ip_listVar
$initNumber $username $ipaddress $active $publicIpClient $status
EOF
            fi
            ((initNumber++))
        fi
    done < $indexEasyRsa
    # sort -t' ' -k2 $ip_list >> $ip_listSorted
    # </getting IP list from index>
    userExist=FALSE
    if [[ ! -z "$ip_listVar" ]]; then
	    echo "---------------- I N F O ------------------"
        column -t -s " " <<< $(printf "$ip_listVar")
        userExist=TRUE
    fi
}

listuserSpin(){
    startSubSpin "Connecting to console..."
    pythonOutput=$(python3 - << EOF
import unicodedata
import re
import getpass
import telnetlib
import time
import os
from decouple import config

HOST = unicodedata.normalize('NFKD', config('management_host')).encode('ascii', 'ignore')
PORT = unicodedata.normalize('NFKD', config('management_port')).encode('ascii', 'ignore')

# timeout = 30
# user = input("Enter your remote account: ")
# password = getpass.getpass("User Password: ")
# enable = getpass.getpass("Enable Password: ")

tn = telnetlib.Telnet(HOST,PORT)

tn.write(b"status\n")
# tn.write(b"exit\n")
time.sleep(3)
s = tn.read_very_eager()
string_output = s.decode('ascii')
print(string_output)
tn.close()
EOF
)

    list_=$(printf "$pythonOutput"  | sed '1,4d' | sed '/^ROUTING/,$d' | sed 's/\:.*$//g')
    startSubSpin "Identifying connectivity..."
    initNumber=1
    while IFS= read -r l; do
        if ! echo $l | grep -qEo "^.*/CN=$serverCN$"; then
            ipaddress=""
            username=$(echo $l | sed -rn 's/^.*\/CN=([A-Za-z0-9]*)$/\1/p')
            active="No"
            publicIpClient="N/A"

            if [ -f "$ccd/$username" ]
            then
                ipaddress=$(cat "$ccd/$username" | grep -Eo '\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b' | head -n1)
            else
                ipaddress="no_ip_address_assigned"
            fi

            status=""

            if grep -qEo '^R' <<< $l
            then
                status="Revoke"
            else
                status="Valid"
            fi

            # check if user is online
            for list in $list_; do
                usernameFromList=$(echo $list | cut -d"," -f1)
                publicipClientFromList=$(echo $list | cut -d"," -f2)
                if [[ $username == $usernameFromList ]]; then
                    active="Yes"
                    publicIpClient=$(echo $publicipClientFromList)
                    break
                fi
            done
            if [ -z "$ip_listVar" ]; then
                read -r -d '' ip_listVar <<EOF
# Username IP Active Public Status
- -------- -- ------ ------ ------
$initNumber $username $ipaddress $active $publicIpClient $status
EOF
            else
                read -r -d '' ip_listVar <<EOF
$ip_listVar
$initNumber $username $ipaddress $active $publicIpClient $status
EOF
            fi
            ((initNumber++))
        fi
    done < $indexEasyRsa
    # sort -t' ' -k1 $ip_list >> $ip_listSorted
    # </getting IP list from index>
    stopSpinSuccess "All done!"
    userExist=FALSE
    if [ ! -z "$ip_listVar" ]; then
	    echo "---------------- I N F O ------------------"
        column -t -s " " <<< $(printf "$ip_listVar")
        userExist=TRUE
    fi
}

listUserMenu(){
    banner
    checkReq
    varChecker
    listuserSpin
    liner
}