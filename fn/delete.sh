delete(){
    banner
    checkReq
    varChecker
	[ -f "$SCRIPT_DIR/fn/listuser.sh" ] && source "$SCRIPT_DIR/fn/listuser.sh"
    listuserSpin
    if [ -z "$ip_listVar" ]; then
        error "There's no users yet"
        exit 0
    fi
    liner
	LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter number of client you want to delete\n      or press $(yellow enter) to exit: " "clientNumber" "exit"
        [ $clientNumber != "exit" ] || exit 0
        runCmdLoop "grep -qP \"^$clientNumber\\s\" <<< \"\$ip_listVar\" " "Invalid number. Enter valid number of OpenVPN client" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
    done
    username=$(grep -P "^$clientNumber\s" <<< "$ip_listVar" | cut -d' ' -f2)
    for i in {1..3}; do
        areYouSureLoop=TRUE
        while [ $areYouSureLoop == "TRUE" ]; do
            warning=$(yellow "Deleting username will disconnect the user")
            warning "$warning"
            interactive "Are you sure you want to delete $username(y/N)? [$i]\n      or leave it empty to exit: " "areYouSure" "n"
            runCmdLoop "echo $areYouSure | grep -qP \"^(y|Y|yes|Yes|YEs|YES|yES|yeS|yEs|n|N|no|No|NO|nO)\$\" " "Please write Yes or No" "areYouSureLoop"
            if [ $areYouSureLoop == "FALSE" ]; then cursorGoesToUp 3; else cursorGoesToUp 4; fi
        done
	    if [[ "$areYouSure" =~ ^(no|n)$ ]]; then exit 1; fi
    done
    startSpin "Starting to remove $username"
        startSubSpin "Removing $username from index file"
        usernameLineNumberOnIndex=$(grep -nEo '^.*\/CN='$username'$' $indexEasyRsa | head -n1 | cut -d: -f1)
        if [ ! -z $usernameLineNumberOnIndex ]; then sed -i "${usernameLineNumberOnIndex}d" $indexEasyRsa; fi
        startSubSpin "Removing associated files belongs to $username"
        if [ -f "$ccd/$username" ]; then rm "$ccd/$username"; fi
        if [ -f "$privateKey/$username.key" ]; then rm "$privateKey/$username.key" ; fi
        if [ -f "$privateCert/$username.crt" ]; then rm "$privateCert/$username.crt" ; fi
        if [ -f "$privateReq/$username.req" ]; then rm "$privateReq/$username.req" ; fi
        if [ -f "$privateInline/$username.inline" ]; then rm "$privateInline/$username.inline" ; fi
        if [ -f "$clientConfig/$username.ovpn" ]; then rm "$clientConfig/$username.ovpn" ; fi
        if [ -f "$clientConfig/$username.pem" ]; then rm "$clientConfig/$username.pem" ; fi
        if [ -f "$dirUserReadConfig/$username.ovpn" ]; then rm "$dirUserReadConfig/$username.ovpn" ; fi
        startSubSpin "Removing associated route push belongs to $username"
        if [ -f "$route" ]; then
            sed -i "/#$username#/d" "$route"
            sed -i '/^$/d' "$route"
        fi
    stopSpinSuccess "Removing $username success!"
    cd "$easyrsa"
    startSubSpin "Reloading services"
        runCommandExitMessage "./easyrsa gen-crl" "Generating new CRL" "Generating CRL success!" "Generating CRL failed!"
        cp $easyrsaCrl $etc
        runCommandExitMessage "systemctl restart openvpn@$serverCN.service" "Restarting service" "Restarting server success!" "Restarting server failed!"
    stopSpinSuccess "$username successfully removed!"
    # remove from pki
    # remove private key
    # remove private certificate
    # remove private crs
    # remove configuration (root)
    # remove configuration (regular user)
    # remove ccd
    # generate new crl
}