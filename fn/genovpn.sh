genconfig(){
	banner
	checkReq
	varChecker
	[ -f "$SCRIPT_DIR/fn/listuser.sh" ] && source "$SCRIPT_DIR/fn/listuser.sh"
	listuserSpin
	liner
	infoAppendString "The list above is list of OpenVPN user as reference\n      Use different username and last octet IP"
	randomUsername=$(randomCharacter)
	LOOPINTERACTIVE=TRUE
	while [ $LOOPINTERACTIVE == "TRUE" ]; do
		interactive "Enter username (don't use same username)\n      or leave it empty to use $(yellow $randomUsername) as username: " "username" "$randomUsername"
		runCmdLoop "! grep -qEo '^.*\/CN='$username'\$' \"$indexEasyRsa\" && [ ! -f \"$privateKey/$username.key\" ] && [ ! -f \"$privateCert/$username.crt\" ] && [ ! -f \"$ccd/$username\" ]" "Failed to generate account. $(yellow $username) already generated" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
	done
	infoAppendString "Your username is: $(green $username)"
	loopIpDef=2
	loopIpDefBool=TRUE
	while [ $loopIpDefBool == "TRUE" ]; do
		if [ $loopIpDef -eq 254 ]; then
			stopSpinFailedExit1 "It's reach maximum IP range"
			break;
		fi
		tonum $localIp localIpNum
		clientLocalIpNum=$((localIpNum+loopIpDef))
		toaddr $clientLocalIpNum clientLocalIp
		if ls $ccd/* > /dev/null 2>&1; then
			freeIpFound=TRUE
			for f in $ccd/* ; do
				IP=$(grep -Po "((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}" $f | head -n1)
				if [ "$IP" = "$clientLocalIp" ]; then
					freeIpFound=FALSE
					break
				fi
			done
			if [ $freeIpFound == "TRUE" ]; then break; else ((loopIpDef++)); fi
		else
			loopIpDefBool=FALSE
		fi
	done
	LOOPINTERACTIVE=TRUE
	while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter last octet of IP address (don't use same last octet of above IP)\n      or leave it empty to use $(yellow $loopIpDef) as last octet: " "ipLastOctet" "$loopIpDef"
		if echo $ipLastOctet | grep -qP "^([2-9]|[1-9]\d|1\d\d|2[0-4]\d|25[0-4])$"; then
			tonum $localIp localIpNum
			clientLocalIpNum=$((localIpNum+ipLastOctet))
			toaddr $clientLocalIpNum clientLocalIp
			if ls $ccd/* > /dev/null 2>&1; then
				ipAlreadyUse=FALSE
				for f in $ccd/* ; do
					IP=$(grep -Po "((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}" $f | head -n1)
					if [ "$IP" = "$clientLocalIp" ]; then
						error "$clientLocalIp has been used"
						sleep 2
						ipAlreadyUse=TRUE
						break
					fi
				done
			else 
				LOOPINTERACTIVE=FALSE
				cursorGoesToUp 2
				break
			fi
			if [ $ipAlreadyUse == "FALSE" ]; then LOOPINTERACTIVE=FALSE; fi
		else
			error "Last octet out of range. enter between 2 - 254"
			sleep 2
		fi
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
	done
	infoAppendString "Your OpenVPN local IP is: $(green $clientLocalIp)"
	LOOPINTERACTIVE=TRUE
	while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Do you want to route all traffic through OpenVPN?(y/N)\n      or leave it empty to bypass traffic: " "routeTrafficInt" "n"
		runCmdLoop "echo $routeTrafficInt | grep -qP \"^(y|Y|yes|Yes|YEs|YES|yES|yeS|yEs|n|N|no|No|NO|nO)\$\" " "Please write Yes or No" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
	done
	routeTraffic=${routeTrafficInt,,}
	if [[ "$routeTraffic" =~ ^(yes|y)$ ]]; then stringDefRoute="Yes"; else stringDefRoute="No"; fi
	infoAppendString "Route Traffic Through OpenVPN: $(green $stringDefRoute)"
	cd $easyrsa
	runCommandExitMessage "./easyrsa build-client-full $username nopass" "Generating certificate and private key" "Generating certificate and private key success" "Generating certificate and private key failed"
	generateConfig $username $clientLocalIp $routeTraffic
	sleep 1
	cursorGoesToUp 1
	infoAppendString "Configuration location: $(green $clientConfig/$username.ovpn)"
	scpCommand=$(green "scp -P $sshPort $shellUserReadConfig@$sshAddress:$dirUserReadConfig/$username.ovpn .")
	infoAppendString "Use the following scp command:\n      $scpCommand"
	exit 0
}

## generate config
generateConfig(){ #<username> <last-octet> <default route[y/N]>
    defaultRouteString=""
    username=$1
    clientLocalIp=$2
    defaultRoute="${3,,}"
    if [[ "$routeTraffic" =~ ^(yes|y)$ ]]; then
        defaultRouteString="redirect-gateway def1"
    fi
    sleep 1
	if [ $tlsStatus == "no" ]; then
		keydirection_server=''
        tlskey=''
	else
	    read -r -d '' tlskey << EOF
<tls-auth>
$(cat $ta)
</tls-auth>
EOF
        keydirection_server="key-direction 1"
    fi

	cat << EOF > $ccd/$username
ifconfig-push $clientLocalIp 255.255.255.0
EOF

    cat << EOF > $clientConfig/$username.ovpn
# ------------- C O P Y  H E R E -------------
client
nobind
pull
dev tun
user nobody
group nogroup
persist-tun
persist-key
remote $publicIp $port $protocol
verb 3
auth SHA1
cipher AES-128-CBC
data-ciphers AES-128-CBC
remote-cert-tls $serverCN
$keydirection_server
$defaultRouteString
<ca>
$(cat $easyrsa/pki/ca.crt)
</ca>
<key>
$(cat $easyrsa/pki/private/$username.key)
</key>
<cert>
$(cat $easyrsa/pki/issued/$username.crt)
</cert>
$tlskey

# ------------- E N D S  H E R E -------------
EOF

if [ ! -z $shellUserReadConfig ] && [ ! -z $dirUserReadConfig ] && grep -qP "^$shellUserReadConfig:" /etc/passwd && [ -d $dirUserReadConfig ]; then
	cp $clientConfig/$username.ovpn $dirUserReadConfig
	chown $shellUserReadConfig:$shellUserReadConfig $dirUserReadConfig/$username.ovpn
fi

    cat << EOF > $clientConfig/$username.pem
# ------------- C O P Y  H E R E -------------
<cert>
$(cat $easyrsa/pki/issued/$username.crt)
</cert>

<key>
$(cat $easyrsa/pki/private/$username.key)
</key>

# ------------- E N D S  H E R E -------------
EOF
}