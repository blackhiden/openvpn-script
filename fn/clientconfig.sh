readClientConfig(){
    banner
    checkReq
    varChecker
	[ -f "$SCRIPT_DIR/fn/listuser.sh" ] && source "$SCRIPT_DIR/fn/listuser.sh"
    listuserSpin
    if [ -z "$ip_listVar" ]; then
        error "There's no users yet"
        exit 0
    fi
    liner
	LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter number of client you want to read\n      or press $(yellow enter) to exit: " "clientNumber" "exit"
        [ $clientNumber != "exit" ] || exit 0
        runCmdLoop "grep -qP \"^$clientNumber\\s\" <<< \"\$ip_listVar\" " "Invalid number. Enter valid number of OpenVPN client" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
    done
    username=$(grep -P "^$clientNumber\s" <<< "$ip_listVar" | cut -d' ' -f2)
    runCommandExitMessage "[ -f $clientConfig/$username.ovpn ]" "Retrieving configuration $(yellow $username)" "Retrieving client $(yellow $username) config files success" "Retrieving client $(yellow $username) config files failed"
    clear
    cat $clientConfig/$username.ovpn
    exit 0
}