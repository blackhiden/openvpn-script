listRoutes(){
    banner
    checkReq
    varChecker
    netPushList="# Username IP Subnetmask"
    read -r -d '' netPushList <<EOF
# Username IP Subnetmask
- -------- -- ----------
EOF
    initNumber=1
    while IFS= read -r l; do
        if ! echo $l | grep -qP "CN=$serverCN"; then
            username=$(echo $l | sed -rn 's/^.*\/CN=([A-Za-z0-9]*)$/\1/p')
            if [ -f "$ccd/$username" ] && grep -qP "iroute" "$ccd/$username"; then
                # ipaddress=$(cat "$ccd/$username" | grep -Eo '\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b' | head -n1)
                while IFS= read -r m; do
                    strToArr "$m" "strArray"
                    network=${strArray[1]}
                    nm=${strArray[2]}
                    ipsubnet $network $nm - - netnum
                    tonum $nm nmnum
                    read -r -d '' netPushList <<EOF
$netPushList
$initNumber $username $network $nm
EOF
                    read -r -d '' netList <<EOF
$netList
$initNumber $username $network $nm $netnum $nmnum
EOF
                    ((initNumber++))
                done <<< $(grep "iroute" "$ccd/$username")
            fi
        fi
    done < $indexEasyRsa
    echo "---------------- I N F O ------------------"
    column -t -s " " <<< $(printf "$netPushList")
    userExist=TRUE
    liner
}

pushRoute(){
    userRoute=$1
    prefixRoute=$2
    banner
    infoAppendString "This menu will help you to push route\n      a network to other client so all clients\n      can connect inside the network"
    checkReq
    varChecker
	[ -f "$SCRIPT_DIR/fn/listuser.sh" ] && source "$SCRIPT_DIR/fn/listuser.sh"
    listuserSpin
    if [ -z "$ip_listVar" ]; then
        error "There's no users yet"
        exit 0
    fi
    liner
    LOOPINTERACTIVE=TRUE
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter the number of client you want to route into\n      or press $(yellow enter) to exit: " "clientNumber" "exit"
        [ $clientNumber != "exit" ] || exit 0
        runCmdLoop "grep -qP \"^$clientNumber\\s\" <<< \"\$ip_listVar\" " "Invalid number. Enter valid number of OpenVPN client" "LOOPINTERACTIVE"
        usernameRouteInto=$(grep -P "^$clientNumber\s" <<< "$ip_listVar" | cut -d' ' -f2)
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; fi
        infoAppendString "The network you're going to push belongs to: $(green $usernameRouteInto)"
    done
    userLocalIp=$(grep -P "^$clientNumber\s" <<< "$ip_listVar" | cut -d' ' -f3)
    while [ -z $netExistLoop ] || [ $netExistLoop == "TRUE" ]; do
        interactive "Enter IP address and CIDR like the following example format: $(yellow 192.168.10.0/24)\n      type here: " "ipCidr" "exit"
        runCmdLoop "echo \"$ipCidr\" | grep -qP \"^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}\/([1-9]|[12][0-9]|3[02])\$\" " "Invalid IP/CIDR format" "formatLoop"
        if [ $formatLoop == "TRUE" ]; then cursorGoesToUp 3; continue; fi
        ipNetworkInput=$(echo $ipCidr | cut -d'/' -f1)
        cidr=$(echo $ipCidr | cut -d'/' -f2)
        tonum $ipNetworkInput ipNetworkInputNum
        prefixtonetmask $cidr netmask netmaskNum
        ipsubnet $ipNetworkInput $netmask ipNetwork - ipNetworkNum
        runCmdLoop "[ $ipNetworkNum == $ipNetworkInputNum ]" "IP you're entered is not a valid network" "netValidLoop"
        if [ $netValidLoop == "TRUE" ]; then cursorGoesToUp 3; continue; fi
        while IFS= read -r netLine; do
            netIdEachLine=$(echo $netLine | sed 's/ .*$//')
            netPerline=$(echo $netIdEachLine | sed 's/\/.*$//')
            if echo $netIdEachLine | grep -qP '\/'; then
                cidrPerLine=$(echo $netIdEachLine | cut -d'/' -f2)
            else
                cidrPerLine=32
            fi
            prefixtonetmask $cidrPerLine nm nmnum
            ipsubnet $netPerline $nm - - netnum bcnum
            if [ $ipNetworkInputNum -ge $netnum ] && [ $ipNetworkInputNum -le $bcnum ]; then
                error "Network already exist in $(yellow 'ip route show') system"
                sleep 3
                netExistLoop=TRUE
                break
            else
                netExistLoop=FALSE
            fi
        done <<< $( ip route show | grep -v default)
        if ls -A $ccd/* > /dev/null 2>&1; then
            while IFS= read -r ccdLine; do
                if grep -qP "iroute" $ccdLine; then
                    if [ -z "$newString" ]; then
                        newString="$(grep "iroute" $ccdLine)"
                    else
                        read -r -d '' newString << EOF
                        $newString
                        $(grep "iroute" $ccdLine)
EOF
                    fi
                fi
            done <<< $(ls -A $ccd/*)
        fi
        while IFS= read -r newStringLine; do
            stripped=$(echo $newStringLine | sed 's/^iroute //')
            irouteNet=$(echo $stripped | cut -d' ' -f1)
            irouteNm=$(echo $stripped | cut -d' ' -f2)
            ipsubnet $irouteNet $irouteNm - - irouteNetNum irouteBcNum
            if [ $ipNetworkNum -ge $irouteNetNum ] && [ $ipNetworkNum -le $irouteBcNum ]; then
                error "Network already exist in one of client's network. Please use another"
                sleep 3
                netExistLoop=TRUE
                break
            else
                netExistLoop=FALSE
            fi
        done <<< $newString
        if [ $netExistLoop == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; continue; fi
        infoAppendString "This will push $(green $ipNetwork/$cidr) belongs to $(green $usernameRouteInto) \n      to all connected clients"
    done
    ccdUser=$ccd/$usernameRouteInto
    cat <<EOF >> $route

push "route $ipNetwork $netmask" #$usernameRouteInto#$ipNetwork#$netmask#$ipNetworkNum#$netmaskNum
route $ipNetwork $netmask $userLocalIp #$usernameRouteInto#$ipNetwork#$netmask#$ipNetworkNum#$netmaskNum

EOF

cat <<EOF >> $ccdUser

iroute $ipNetwork $netmask #$usernameRouteInto#$ipNetwork#$netmask#$ipNetworkNum#$netmaskNum

EOF
    runCommandExitMessage "systemctl restart openvpn@$serverCN.service" "Restarting system" "Restarting system success" "Restarting system failed"
    success "Network have been pushed into system"
}

deleteRoute(){
    listRoutes
    while [ $LOOPINTERACTIVE == "TRUE" ]; do
        interactive "Enter the number of network client you want to delete\n      or press $(yellow enter) to exit: " "clientNumber" "exit"
        [ $clientNumber != "exit" ] || exit 0
        runCmdLoop "grep -qP \"^$clientNumber\\s\" <<< \"\$netList\" " "Invalid number. Enter valid number of OpenVPN client" "LOOPINTERACTIVE"
        if [ $LOOPINTERACTIVE == "FALSE" ]; then cursorGoesToUp 2; else cursorGoesToUp 3; continue; fi
        usernameNumberDelete=$(grep -P "^$clientNumber\s" <<< "$netList" | cut -d' ' -f2)
        choosenString=$(grep -P "^$clientNumber\s" <<< "$netList")
        strToArr "$choosenString" "choosenArray"
        choosenNet=${choosenArray[2]}
        choosenBc=${choosenArray[3]}
        choosenNetNum=${choosenArray[4]}
        choosenBcNum=${choosenArray[5]}
    done
    infoAppendString "The network you're going to delete is\n      $(green $choosenNet) $(green $choosenBc) belongs to:\n      $(green $usernameNumberDelete)" 
}