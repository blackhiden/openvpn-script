<div id="top"></div>
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<!-- [![Contributors][contributors-shield]][contributors-url] -->
<!-- [![Forks][forks-shield]][forks-url] -->
<!-- [![Stargazers][stars-shield]][stars-url] -->
<!-- [![Issues][issues-shield]][issues-url] -->
<!-- [![MIT License][license-shield]][license-url] -->
<!-- [![LinkedIn][linkedin-shield]](https://www.linkedin.com/in/blackhiden/) -->



<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/blackhiden/openvpn-script">
    <img src="images/logo.jpg" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">OpenVPN Script</h3>

  <p align="center">
    These scripts will help you to deploy OpenVPN instance, including create, view existing, active and status client.
    <br />
    <a href="https://gitlab.com/blackhiden/openvpn-script"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <!-- <a href="https://github.com/github_username/repo_name">View Demo</a> -->
    <!-- <a href="https://github.com/github_username/repo_name/issues">Report Bug</a> -->
    <!-- <a href="https://github.com/github_username/repo_name/issues">Request Feature</a> -->
  </p>
</div>

<!-- Please Enable or Disable readme here. I'm just too lazy to finish README.md -->

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
    <li><a href="#thanks-to">Thanks to</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

<!-- [![Product Name Screen Shot][product-screenshot]](https://example.com) -->

This project was made to simplify the installation and maintain of OpenVPN server/client.
<!-- Here's a blank template to get started: To avoid retyping too much info. Do a search and replace with your text editor for the following: `github_username`, `repo_name`, `twitter_handle`, `linkedin_username`, `email_client`, `email`, `project_title`, `project_description` -->

<p align="right">(<a href="#top">back to top</a>)</p>

### Built With

* [Bash](https://www.gnu.org/software/bash/)
* [Python](https://www.python.org/)

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- GETTING STARTED -->
## Getting Started
These scripts work perfectly fine (and only) under debian distribution (tested on Debian Buster 10 and Ubuntu Focal Fossa 20.04). The configuration use `subnet` as topology. It means you'll get fully /24 block routable network. Since it use `subnet` protocol, parameter `tcp-server` and `tcp-client` seem to be not right. It's not mentioned directly in manual, but this thread will explain ([What is the difference between tcp/tcp-server/tcp-client?](https://forums.openvpn.net/viewtopic.php?t=7168))

### Prerequisites
Make sure you have the following packages installed:
* python
  ```sh
  apt update && apt install python3.12
  ```
* telnet
  ```sh
  apt update && apt install telnet
  ```

I believe those packages are installed automatically
* Of course you need full `root` access because these scripts never work with sudo


### Installation

1. First thing first, you have to clone this repository:
   ```sh
   git clone https://gitlab.com/blackhiden/openvpn-script
   ```

2. Enter the directory and copy .env.example
   ```sh
   cd openvpn-script
   ```

3. Grant execute permission to all *.sh and *.py 
   ```sh
   chmod +x ovpn-script.sh
   ```

4. Open the script
   ```sh
   ./ovpn-script.sh
   ```
5. Choose install server and follow the instructions.

<p align="right">(<a href="#top">back to top</a>)</p>

<!-- USAGE EXAMPLES -->
## Usage

To make it easier I decide to make this tool interactive. So you don't have to memorize all argument just like previous version. All options will available once you finish install the server.

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Twitter: [@blackhiden](https://twitter.com/blackhiden)

Facebook: [Denny Lastpaste](https://facebook.com/blackhiden)

Instagram: [@blackhiden](https://instagram.com/blackhiden)

Email: [blackhiden@gmail.com](mailto:blackhiden@gmail.com)

Blog: [Everything - It's all about me, computer and my life](https://blackhiden.blogspot.com/)

Project Link: [https://gitlab.com/blackhiden/openvpn-script](https://gitlab.com/blackhiden/openvpn-script)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

<!-- * []() -->

<p align="right">(<a href="#top">back to top</a>)</p>

## Thanks to
I'm so dumb that I have to copy this amazing README.md template. Thanks to  [Othneil Drew](https://github.com/othneildrew/Best-README-Template)
<!-- * []() -->

<p align="right">(<a href="#top">back to top</a>)</p>

