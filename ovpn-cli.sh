#!/bin/bash
mainPid=$$
LOOPINTERACTIVE=TRUE
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
scriptLocation=${SCRIPT_DIR:-/root/ovpn}
logfile="$scriptLocation/server.log"
env="$SCRIPT_DIR/.env"
fninstall="$SCRIPT_DIR/fn-install.sh"
d=$(date '+%a, %d %B %Y - %H:%M:%S')
[ -f "$logfile" ] && echo "[$d]" > "$logfile"

checkReq(){
    version=$(grep '^NAME' /etc/os-release)
    version=${version,,}
    ubuntu='Ubuntu'
    debian='Debian'
    
    if  ! $(echo $version | grep -q 'ubuntu') && ! $(echo $version | grep -q 'debian') ; then
        echo "Your operating system is not Ubuntu nor Debian"
        exit 1
    fi

    : ${SUDO_USER:=nosudo}
    if [ $SUDO_USER != "nosudo" ]; then
        echo "Script run by sudo. You should become root"
        exit 1
    fi

    if [ $EUID -ne 0 ]; then
        echo "You're not root"
        exit 1
    fi
}

varChecker(){
    if [ -f "$env" ]; then
        if source "$env"; then
            if  [ -z $publicIp ] && 
                [ -z $localIp ] && 
                [ -z $protocol ] && 
                [ -z $port ] && 
                [ -z $configDirectory ] && 
                [ -z $tlsStatus ] && 
                [ -z $serverCN ] && 
                [ -z $etc ] && 
                [ -z $ccd ] && 
                [ -z $route ] && 
                [ -z $ta ] && 
                [ -z $crl ] && 
                [ -z $clientConfig ] && 
                [ -z $easyrsaGit ] && 
                [ -z $easyrsa ] && 
                [ -z $easyrsaCrl ] && 
                [ -z $indexEasyRsa ] && 
                [ -z $privateKey ] && 
                [ -z $privateCert ] && 
                [ -z $privateReq ] && 
                [ -z $management_host ] && 
                [ -z $management_port ] && 
                [ -z $logfile ] && 
                [ -z $tlsStatus ] &&
                [ -z $shellUserReadConfig ] && 
                [ -z $dirUserReadConfig ]; then
                    echo "Some of variable are gone. Please see .env.example as reference"
                    exit 1
            fi
        else
            echo "Sourcing failed"
            exit 1
        fi
    else
        echo "$env not found"
        exit 1
    fi
}

if [ -z $1 ]; then
    echo "need param(s)"
    exit 1
fi

checkReq
varChecker
case $1 in
    ls) source "$SCRIPT_DIR/cli/listuser.sh" && listuser ;;
    *) exit 0;;
esac

