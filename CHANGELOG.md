# Changelog
## v2.1.0
### ADDED
- New feature delete user

## v2.0.2

### ADDED
- nothing to added

### UPDATED
- fix ip filter

### REMOVED
- Nothing removed

## v2.0.1

### ADDED
- Nothing added

### UPDATED
- function name
- offline and online installation mode

## v2.0.0

### ADDED
- active.sh to show active user
- default value on online install
- delete user
- route user
- pop route

### UPDATED
- Spinner goes to subscript
- verbose
- regenarte new crl
- change file name
- revoke user
- unrevoke
- uninstall

### REMOVED
- Nothing removed


## v1.3.0

### Added or Changed
- active.sh to show active user

### Removed
- Nothing removed

## v1.2.0

### Added or Changed
- Add push route

### Removed
- Nothing removed


# Changelog

## v1.1.0

### Added or Changed
- Add OpenVPN Uninstaller. This feature only stop & disable the server, and then remove easy rsa init.

### Removed
- Nothing removed


## v1.0.0

### Added or Changed
- Inital release
- Nothing added or changed

### Removed
- Nope